const Bundler = require('parcel-bundler');
const express = require("express");
const app = express();

/*************************/
const PORT = process.env.PORT || 3000;
/*************************/
//BUNDLER
const file = './src/index.html'; // Pass an absolute path to the entrypoint here
const options = {};
const bundler = new Bundler(file, options);
app.use(bundler.middleware());


// Routes

    app.get('/', (req, res) => {
        res.send('HELLO ');
    });

/*************************/
app.listen(PORT, () => {
    console.log(`App is running at http://localhost:${PORT}`);
});